package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JTabbedPane;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JTextPane;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import Controller.AdministradorCTRL;
import Controller.CursoCTRL;
import Controller.EstudanteCTRL;
import Controller.SimuladoCTRL;
import Controller.UniversidadeCTRL;
import javax.swing.JScrollBar;
import javax.swing.DropMode;

public class TelaPrincipal extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtDtNscAdm;
	private JTextField txtNomeAluno;
	private JTextField txtDtNasc;
	private JTextField txtNomeAdm;
	private JTextField txtDtNascAluno;
	private JTextField textField;
	private JTextField txtLocalAdm;
	private JTextField txtLocalAluno;
	private JTextField textField_1;
	private JTextField txtSenhaAdm;
	private JTextField txtLoginAluno;
	private JTextField textField_2;
	private JTextField txtSenhaAluno;
	private JTextField textField_3;
	private JTextField txtEmailAdm;
	private JTextField textField_4;
	private JTextField txtLoginAdm;
	private JTextField txtEmailAuno;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textNomeCurso;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JLabel lblNomeAluno;
	private JLabel lblDtNascAluno;
	private JButton btnCadastrarAluno;
	private JLabel lblLocalAluno;
	private JLabel lblLoginAluno;
	private JLabel lblSenhaAluno;
	private JLabel lblEmailAluno;
	private JLabel lblNomeAdm;
	private JLabel lblDtNascAdm;
	private JButton btnCadastrarAdm;
	private JLabel lblCadastroDeAdm;
	private JLabel lblLocalAdm;
	private JLabel lblLoginAdm;
	private JLabel lblSenhaAdm;
	private JLabel lblEmailAdm;
	private JTextField txtAreaSim;
	private JTextField txtVestSimu;
	private JTextField textValorQ;
	private JLabel lblQuesto;
	private JLabel lblGabarito;
	private JTextField txtGaba;
	private JButton btnPesquisar_1;
	private JTextField txtCodSimulado;
	private JTextField textIdAdministrador;
	private JButton btnPesquisar_3;
	private JTextField textCodUniversidade;
	private JButton btnPesquisar_4;
	private JTextField textCodCurso;
	private JLabel lblNewLabel_1;
	private JLabel lblCdigo_1;
	private JLabel lblCdigo_2;
	private JTextArea JTextArea;
	private JTextField txtIdAdm;
	private JTextField txtCodCurso;
	private JTextField txtCodUniversidade;
	private JButton btnPesquisar_5;
	private JButton btnPesquisar_6;
	private JButton btnPesquisar_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPrincipal frame = new TelaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaPrincipal() {
		setForeground(new Color(255, 255, 255));
		setTitle("Ran\u00C7oftware\r\n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 671, 567);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 0, 102));
		panel.setBounds(0, 0, 655, 528);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 107, 655, 421);
		panel.add(tabbedPane);
		
		JPanel Alunos = new JPanel();
		Alunos.setForeground(new Color(255, 51, 255));
		tabbedPane.addTab("Alunos", (Icon) null, Alunos, null);
		tabbedPane.setForegroundAt(0, Color.BLACK);
		Alunos.setLayout(null);
		

		lblNomeAluno = new JLabel("Nome:");
		lblNomeAluno.setBounds(40, 40, 46, 14);
		Alunos.add(lblNomeAluno);
		
		lblDtNascAluno = new JLabel("Data de Nascimento:");
		lblDtNascAluno.setBounds(40, 65, 126, 14);
		Alunos.add(lblDtNascAluno);
		
		txtNomeAluno = new JTextField();
		txtNomeAluno.setBounds(163, 37, 240, 20);
		Alunos.add(txtNomeAluno);
		txtNomeAluno.setColumns(10);
		
		txtDtNascAluno = new JTextField();
		txtDtNascAluno.setBounds(163, 62, 113, 20);
		Alunos.add(txtDtNascAluno);
		txtDtNascAluno.setColumns(10);
		
		btnCadastrarAluno = new JButton("Cadastrar");
		btnCadastrarAluno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CadastrarEstudante();
			}
		});
		btnCadastrarAluno.setBounds(514, 359, 126, 23);
		Alunos.add(btnCadastrarAluno);
		
		JLabel lblCadastroDeAluno = new JLabel("Cadastro de Estudante");
		lblCadastroDeAluno.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblCadastroDeAluno.setBounds(220, -18, 183, 67);
		Alunos.add(lblCadastroDeAluno);
		

		lblLocalAluno = new JLabel("Local:");
		lblLocalAluno.setBounds(40, 90, 46, 14);
		Alunos.add(lblLocalAluno);
		
		txtLocalAluno = new JTextField();
		txtLocalAluno.setBounds(163, 87, 240, 20);
		Alunos.add(txtLocalAluno);
		txtLocalAluno.setColumns(10);
		

		lblLoginAluno = new JLabel("Login:");
		lblLoginAluno.setBounds(40, 140, 46, 14);
		Alunos.add(lblLoginAluno);
		
		txtLoginAluno = new JTextField();
		txtLoginAluno.setBounds(163, 137, 113, 20);
		Alunos.add(txtLoginAluno);
		txtLoginAluno.setColumns(10);
		

		lblSenhaAluno = new JLabel("Senha:");
		lblSenhaAluno.setBounds(40, 165, 46, 14);
		Alunos.add(lblSenhaAluno);
		
		txtSenhaAluno = new JTextField();
		txtSenhaAluno.setBounds(163, 162, 172, 20);
		Alunos.add(txtSenhaAluno);
		txtSenhaAluno.setColumns(10);
		

		lblEmailAluno = new JLabel("Email:");
		lblEmailAluno.setBounds(40, 115, 46, 14);
		Alunos.add(lblEmailAluno);
		
		txtEmailAuno = new JTextField();
		txtEmailAuno.setBounds(163, 112, 240, 20);
		Alunos.add(txtEmailAuno);
		txtEmailAuno.setColumns(10);
		
		JButton btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PesquisaEstudante();
			}
		});
		btnPesquisar.setBounds(400, 359, 89, 23);
		Alunos.add(btnPesquisar);
		
		JPanel Simulado = new JPanel();
		tabbedPane.addTab("Simulado", null, Simulado, null);
		Simulado.setLayout(null);
		
		JButton btnSalvarQuesto = new JButton("Salvar Quest\u00E3o");
		btnSalvarQuesto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CadastrarSimulado();
			}
		});
		btnSalvarQuesto.setBounds(517, 359, 123, 23);
		Simulado.add(btnSalvarQuesto);
		
		JLabel lblNewLabel = new JLabel("Area :");
		lblNewLabel.setBounds(21, 154, 58, 14);
		Simulado.add(lblNewLabel);
		
		txtAreaSim = new JTextField();
		txtAreaSim.setBounds(173, 151, 222, 20);
		Simulado.add(txtAreaSim);
		txtAreaSim.setColumns(10);
		
		JLabel lblVestibular = new JLabel("Vestibular :");
		lblVestibular.setBounds(21, 179, 76, 14);
		Simulado.add(lblVestibular);
		
		txtVestSimu = new JTextField();
		txtVestSimu.setBounds(173, 176, 222, 20);
		Simulado.add(txtVestSimu);
		txtVestSimu.setColumns(10);
		
		JLabel lblValorDaQuesto = new JLabel("Valor da Quest\u00E3o :");
		lblValorDaQuesto.setBounds(21, 204, 128, 14);
		Simulado.add(lblValorDaQuesto);
		
		textValorQ = new JTextField();
		textValorQ.setBounds(173, 201, 222, 20);
		Simulado.add(textValorQ);
		textValorQ.setColumns(10);
		
		lblQuesto = new JLabel("Quest\u00E3o :");
		lblQuesto.setBounds(21, 11, 58, 14);
		Simulado.add(lblQuesto);
		
		lblGabarito = new JLabel("Gabarito :");
		lblGabarito.setBounds(21, 244, 76, 14);
		Simulado.add(lblGabarito);
		
		txtGaba = new JTextField();
		txtGaba.setBounds(135, 241, 86, 20);
		Simulado.add(txtGaba);
		txtGaba.setColumns(10);
		
		btnPesquisar_1 = new JButton("Pesquisar");
		btnPesquisar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PesquisaSimulado();
			}
		});
		btnPesquisar_1.setBounds(405, 359, 89, 23);
		Simulado.add(btnPesquisar_1);
		
		txtCodSimulado = new JTextField();
		txtCodSimulado.setBounds(135, 287, 86, 20);
		Simulado.add(txtCodSimulado);
		txtCodSimulado.setColumns(10);
		
		JLabel lblCdigo = new JLabel("C\u00F3digo:");
		lblCdigo.setBounds(21, 290, 46, 14);
		Simulado.add(lblCdigo);
		
		JTextArea JTextArea = new JTextArea();
		JTextArea.setLineWrap(true);
		JTextArea.setColumns(10);
		JTextArea.setRows(9);
		JTextArea.setBounds(89, 6, 504, 134);
		Simulado.add(JTextArea);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBackground(Color.LIGHT_GRAY);
		scrollBar.setForeground(Color.LIGHT_GRAY);
		scrollBar.setBounds(575, 11, 17, 129);
		Simulado.add(scrollBar);
		
		JPanel Administrador = new JPanel();
		tabbedPane.addTab("Administrador", null, Administrador, null);
		Administrador.setLayout(null);
		

		lblNomeAdm = new JLabel("Nome:");
		lblNomeAdm.setBounds(40, 40, 100, 14);
		Administrador.add(lblNomeAdm);
		

		lblDtNascAdm = new JLabel("Data de Nascimento:");
		lblDtNascAdm.setBounds(40, 65, 123, 14);
		Administrador.add(lblDtNascAdm);
		
		txtDtNscAdm = new JTextField();
		txtDtNscAdm.setBounds(173, 62, 112, 20);
		Administrador.add(txtDtNscAdm);
		txtDtNscAdm.setColumns(10);
		
		txtNomeAdm = new JTextField();
		txtNomeAdm.setBounds(173, 37, 221, 20);
		Administrador.add(txtNomeAdm);
		txtNomeAdm.setColumns(10);
		

		btnCadastrarAdm = new JButton("Cadastrar");
		btnCadastrarAdm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CadastrarAdm();
			}
		});
		btnCadastrarAdm.setBounds(506, 359, 134, 23);
		Administrador.add(btnCadastrarAdm);
		
		JLabel lblCadastroDeAdm;
		lblCadastroDeAdm = new JLabel("Cadastro de Administrador");
		lblCadastroDeAdm.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblCadastroDeAdm.setBounds(209, 3, 237, 25);
		Administrador.add(lblCadastroDeAdm);
		

		lblLocalAdm = new JLabel("Local:");
		lblLocalAdm.setBounds(40, 90, 100, 14);
		Administrador.add(lblLocalAdm);
		
		txtLocalAdm = new JTextField();
		txtLocalAdm.setBounds(173, 87, 221, 20);
		Administrador.add(txtLocalAdm);
		txtLocalAdm.setColumns(10);
		

		lblLoginAdm = new JLabel("Login:");
		lblLoginAdm.setBounds(40, 140, 100, 14);
		Administrador.add(lblLoginAdm);
		
		txtSenhaAdm = new JTextField();
		txtSenhaAdm.setBounds(173, 162, 149, 20);
		Administrador.add(txtSenhaAdm);
		txtSenhaAdm.setColumns(10);
		

		lblSenhaAdm = new JLabel("Senha:");
		lblSenhaAdm.setBounds(40, 165, 100, 14);
		Administrador.add(lblSenhaAdm);
		
		txtLoginAdm = new JTextField();
		txtLoginAdm.setBounds(173, 137, 112, 20);
		Administrador.add(txtLoginAdm);
		txtLoginAdm.setColumns(10);
		
		txtEmailAdm = new JTextField();
		txtEmailAdm.setBounds(173, 112, 221, 20);
		Administrador.add(txtEmailAdm);
		txtEmailAdm.setColumns(10);
		
		lblEmailAdm = new JLabel("Email:");
		lblEmailAdm.setBounds(40, 115, 100, 14);
		Administrador.add(lblEmailAdm);
		
		txtIdAdm = new JTextField();
		txtIdAdm.setBounds(173, 193, 86, 20);
		Administrador.add(txtIdAdm);
		txtIdAdm.setColumns(10);
		
		btnPesquisar_2 = new JButton("Pesquisar");
		btnPesquisar_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PesquisaAdministrador();
			}
		});
		btnPesquisar_2.setBounds(329, 359, 89, 23);
		Administrador.add(btnPesquisar_2);
		
		JLabel lblId = new JLabel("Id:");
		lblId.setBounds(40, 196, 46, 14);
		Administrador.add(lblId);
		
		JPanel Universidade = new JPanel();
		tabbedPane.addTab("Universidade", null, Universidade, null);
		Universidade.setLayout(null);
		
		JLabel lblCadastroDeUniversidade = new JLabel("Cadastro de Universidade");
		lblCadastroDeUniversidade.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblCadastroDeUniversidade.setBounds(223, 8, 237, 25);
		Universidade.add(lblCadastroDeUniversidade);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(24, 47, 63, 14);
		Universidade.add(lblNome);
		
		textField_6 = new JTextField();
		textField_6.setText(" ");
		textField_6.setColumns(10);
		textField_6.setBounds(97, 44, 237, 20);
		Universidade.add(textField_6);
		
		JLabel label_1 = new JLabel("Local:");
		label_1.setBounds(24, 72, 46, 14);
		Universidade.add(label_1);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(97, 69, 237, 20);
		Universidade.add(textField_7);
		
		JLabel label = new JLabel("CEP:");
		label.setBounds(24, 97, 37, 14);
		Universidade.add(label);
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(97, 94, 120, 20);
		Universidade.add(textField_8);
		
		JButton btnCadastrar_1 = new JButton("Cadastrar");
		btnCadastrar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CadastrarUniversidade();
			}
		});
		btnCadastrar_1.setBounds(531, 359, 109, 23);
		Universidade.add(btnCadastrar_1);
		
		txtCodUniversidade = new JTextField();
		txtCodUniversidade.setBounds(97, 125, 86, 20);
		Universidade.add(txtCodUniversidade);
		txtCodUniversidade.setColumns(10);
		
		btnPesquisar_6 = new JButton("Pesquisar");
		btnPesquisar_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PesquisaUniversidade();
			}
		});
		btnPesquisar_6.setBounds(374, 359, 89, 23);
		Universidade.add(btnPesquisar_6);
		
		JPanel Curso = new JPanel();
		tabbedPane.addTab("Curso", null, Curso, null);
		Curso.setLayout(null);
		
		JLabel lblCadastroDeCurso = new JLabel("Cadastro de Curso");
		lblCadastroDeCurso.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblCadastroDeCurso.setBounds(255, 0, 180, 25);
		Curso.add(lblCadastroDeCurso);
		
		JLabel label_3 = new JLabel("Nome:");
		label_3.setBounds(25, 37, 46, 14);
		Curso.add(label_3);
		
		textNomeCurso = new JTextField();
		textNomeCurso.setColumns(10);
		textNomeCurso.setBounds(106, 34, 225, 20);
		Curso.add(textNomeCurso);
		
		JLabel label_4 = new JLabel("Nota de corte:");
		label_4.setBounds(25, 62, 82, 14);
		Curso.add(label_4);
		
		textField_11 = new JTextField();
		textField_11.setColumns(10);
		textField_11.setBounds(106, 59, 114, 20);
		Curso.add(textField_11);
		
		JLabel label_5 = new JLabel("Nota do MEC:");
		label_5.setBounds(25, 87, 82, 14);
		Curso.add(label_5);
		
		textField_12 = new JTextField();
		textField_12.setColumns(10);
		textField_12.setBounds(106, 84, 114, 20);
		Curso.add(textField_12);
		
		JLabel lblCdigoDaUniversidade = new JLabel("C\u00F3digo da Universidade :");
		lblCdigoDaUniversidade.setBounds(25, 112, 151, 14);
		Curso.add(lblCdigoDaUniversidade);
		
		textField_13 = new JTextField();
		textField_13.setColumns(10);
		textField_13.setBounds(186, 109, 57, 20);
		Curso.add(textField_13);
		
		JButton button_1 = new JButton("Cadastrar");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CadastrarCurso();
			}
		});
		button_1.setBounds(539, 359, 101, 23);
		Curso.add(button_1);
		
		txtCodCurso = new JTextField();
		txtCodCurso.setBounds(106, 159, 86, 20);
		Curso.add(txtCodCurso);
		txtCodCurso.setColumns(10);
		
		btnPesquisar_5 = new JButton("Pesquisar");
		btnPesquisar_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PesquisaCurso();
			}
		});
		btnPesquisar_5.setBounds(409, 359, 89, 23);
		Curso.add(btnPesquisar_5);
	}
public void CadastrarAdm(){
		
		AdministradorCTRL c = new AdministradorCTRL();
		
		String login = txtLoginAdm.getText();
		String senha = txtSenhaAdm.getText();
		String nome = txtNomeAdm.getText();
		String dataNasc = txtDtNscAdm.getText();
		String local = txtLocalAdm.getText();
		String email = txtEmailAdm.getText();
				
		c.CadastrarAdm(login,senha,nome,dataNasc,local,email);
	}
public void CadastrarCurso(){
	
	CursoCTRL c = new CursoCTRL();
	
	String nome = textNomeCurso.getText();
	float notaCorte = Float.parseFloat(textField_11.getText());
	int notaMec = Integer.parseInt(textField_12.getText());
	int universidade = Integer.parseInt(textField_13.getText());
			
	c.CadastrarCurso(nome,notaCorte,notaMec,universidade);
}
public void CadastrarEstudante(){
	
	EstudanteCTRL c = new EstudanteCTRL();
	
	String login = txtLoginAluno.getText();
	String senha = txtSenhaAluno.getText();
	String nome = txtNomeAluno.getText();
	String dataNasc = txtDtNascAluno.getText();
	String local = txtLocalAluno.getText();
	String email = txtEmailAuno.getText();
			
	c.CadastrarEstudante(login,senha,nome,dataNasc,local,email);
}
public void CadastrarSimulado(){
	
	SimuladoCTRL c = new SimuladoCTRL();
	
	String area = txtAreaSim.getText();
	String vestibular = txtVestSimu.getText();
	String questao = JTextArea.getText();
	String gabarito = txtGaba.getText();
	float nota = Float.parseFloat(textValorQ.getText());

			
	c.CadastrarSimulado(area, vestibular, questao, gabarito, nota);
}
public void CadastrarUniversidade(){
	
	UniversidadeCTRL c = new UniversidadeCTRL();

	String nome = textField_6.getText();
	String endereco = textField_7.getText();
	int cep = Integer.parseInt(textField_8.getText());
			
	c.CadastrarUniversidade(nome, endereco, cep);
}

public void PesquisaAdministrador(){
	
	AdministradorCTRL c = new AdministradorCTRL();
	
	int ID = Integer.parseInt(txtIdAdm.getText());

	c.PesquisaAdministrador(ID);
	
}

public void PesquisaCurso(){
	
	CursoCTRL c = new CursoCTRL();
	
	int Cod = Integer.parseInt(txtCodCurso.getText());

	c.PesquisaCurso(Cod);
	
}

public void PesquisaEstudante(){
	
	EstudanteCTRL c = new EstudanteCTRL();
	
	int Login = Integer.parseInt(txtLoginAluno.getText());

	c.PesquisaEstudante(Login);
	
}

public void PesquisaSimulado(){
	
	SimuladoCTRL c = new SimuladoCTRL();
	
	int Cod = Integer.parseInt(txtCodSimulado.getText());

	c.PesquisaSimulado(Cod);
	
}

public void PesquisaUniversidade(){
	
	UniversidadeCTRL c = new UniversidadeCTRL();
	
	int Cod = Integer.parseInt(txtCodUniversidade.getText());

	c.PesquisaUniversidade(Cod);
	
}
}