package Controller;
import javax.swing.JOptionPane;
import DAO.TabelaCursoDAO;
import Model.Curso;
public class CursoCTRL {
public void CadastrarCurso(String nome, float notaCorte, int notaMec, int cod_Universidade){
		
		Curso a = new Curso();
		
		a.setNome(nome);
		a.setNotaCorte(notaCorte);
		a.setNotaMec(notaMec);
		a.setCod_Universidade(cod_Universidade);

		
		TabelaCursoDAO dao = new TabelaCursoDAO();
		
		if(dao.Inserir(a)){

			JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
		} else {
			JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
		}

	}
	
	public void PesquisaCurso(int c){
		
		TabelaCursoDAO dao = new TabelaCursoDAO();
		Curso a = dao.PesquisaCurso(c);
		if(a != null){
			JOptionPane.showMessageDialog(null, "Encontrado com suecesso:"
					+ "\nNome do Curso :"+a.getNome()+"\nNota de Corte :"+a.getNotaCorte()+"\nNota do MEC :"+a.getNotaMec());
		} else {
			JOptionPane.showMessageDialog(null, "Erro: Curso nao existe!");
		}
		
	}
}
