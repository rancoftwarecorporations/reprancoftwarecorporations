package Controller;
import javax.swing.JOptionPane;

import DAO.TabelaSimuladoDAO;
import Model.Simulado;
public class SimuladoCTRL {
public void CadastrarSimulado(String area, String vestibular, String questao, String gabarito, float nota){
		
		Simulado a = new Simulado();
		
		a.setArea(area);
		a.setVestibular(vestibular);
		a.setQuestao(questao);
		a.setGabarito(gabarito);
		a.setNota(nota);

		
		TabelaSimuladoDAO dao = new TabelaSimuladoDAO();
		
		if(dao.Inserir(a)){

			JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
		} else {
			JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
		}

	}
	
	public void PesquisaSimulado(int c){
		
		TabelaSimuladoDAO dao = new TabelaSimuladoDAO();
		Simulado a = dao.PesquisaSimulado(c);
		if(a != null){
			JOptionPane.showMessageDialog(null, "Encontrado com suecesso:"
					+ "\nQuestao :"+a.getQuestao()+"\nVestibular :"+a.getVestibular());
		} else {
			JOptionPane.showMessageDialog(null, "Erro: Questao nao existe!");
		}
		
	}
}
