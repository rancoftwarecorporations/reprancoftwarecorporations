package Controller;
import javax.swing.JOptionPane;

import DAO.TabelaUniversidadeDAO;
import Model.Universidade;
public class UniversidadeCTRL {
public void CadastrarUniversidade(String nome, String endereco, int cep){
		
		Universidade a = new Universidade();
		
		a.setNome(nome);
		a.setEndereco(endereco);
		a.setCEP(cep);
		
		TabelaUniversidadeDAO dao = new TabelaUniversidadeDAO();
		
		if(dao.Inserir(a)){

			JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
		} else {
			JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
		}

	}
	
	public void PesquisaUniversidade(int c){
		
		TabelaUniversidadeDAO dao = new TabelaUniversidadeDAO();
		Universidade a = dao.PesquisaUniversidade(c);
		if(a != null){
			JOptionPane.showMessageDialog(null, "Encontrado com sucesso:"
					+ "\nNome :"+a.getNome()+"\nEndereco :"+a.getEndereco()+"\nCodigo :"+a.getCod());
		} else {
			JOptionPane.showMessageDialog(null, "Erro: Administrador nao existe!");
		}
		
	}
}
