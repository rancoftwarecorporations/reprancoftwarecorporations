package Controller;
import javax.swing.JOptionPane;
import DAO.TabelaAdministradorDAO;
import Model.Administrador;
public class AdministradorCTRL {
public void CadastrarAdm(String login, String senha, String nome, String dataNasc, String local, String email){
		
		Administrador a = new Administrador();
		
		a.setLogin(login);
		a.setSenha(senha);
		a.setNome(nome);
		a.setDataNasc(dataNasc);
		a.setLocal(local);
		a.setEmail(email);

		
		TabelaAdministradorDAO dao = new TabelaAdministradorDAO();
		
		if(dao.Inserir(a)){

			JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
		} else {
			JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
		}

	}
	
	public void PesquisaAdministrador(int c){
		
		TabelaAdministradorDAO dao = new TabelaAdministradorDAO();
		Administrador a = dao.PesquisaAdministrador(c);
		if(a != null){
			JOptionPane.showMessageDialog(null, "Encontrado com suecesso:"
					+ "\nNome :"+a.getNome()+"\nEmail :"+a.getEmail());
		} else {
			JOptionPane.showMessageDialog(null, "Erro: Administrador nao existe!");
		}
		
	}
}
