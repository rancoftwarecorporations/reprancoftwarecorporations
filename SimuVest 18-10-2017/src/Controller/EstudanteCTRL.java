package Controller;
import javax.swing.JOptionPane;
import DAO.TabelaEstudanteDAO;
import Model.Estudante;
public class EstudanteCTRL {
public void CadastrarEstudante(String login, String senha, String nome, String dataNasc, String local, String email){
		
		Estudante a = new Estudante();
		
		a.setLogin(login);
		a.setSenha(senha);
		a.setNome(nome);
		a.setDataNasc(dataNasc);
		a.setLocal(local);
		a.setEmail(email);
		
		TabelaEstudanteDAO dao = new TabelaEstudanteDAO();
		
		if(dao.Inserir(a)){

			JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
		} else {
			JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
		}

	}
	
	public void PesquisaEstudante(int c){
		
		TabelaEstudanteDAO dao = new TabelaEstudanteDAO();
		Estudante a = dao.PesquisaEstudante(c);
		if(a != null){
			JOptionPane.showMessageDialog(null, "Encontrado com suecesso:"
					+ "\nNome :"+a.getNome()+"\nEmail :"+a.getEmail());
		} else {
			JOptionPane.showMessageDialog(null, "Erro: Estudante nao existe!");
		}
		
	}
}
