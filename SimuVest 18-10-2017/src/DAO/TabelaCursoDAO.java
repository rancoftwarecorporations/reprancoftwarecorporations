package DAO;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import Controller.FabricadeConexao;
import Model.Curso;
import Model.ModeloMySql;


public class TabelaCursoDAO {
	
private ModeloMySql modelo = null;
	
	public TabelaCursoDAO(){
		
		modelo = new ModeloMySql();
		
		/* o que eu preciso para conectar no banco */
		modelo.setDatabase("rancoftware");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");

	}


	public boolean Inserir(Curso curso){

		boolean status = true;
		Connection con = FabricadeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
		JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
		return false;
		}	

		/* prepara o sql para inserir no banco */
		
		String sql = "INSERT INTO curso (nome, notamec, notacorte, cod_universidade) "
				+ "VALUES(?,?,?,?);";

		
		
		/* prepara para inserir no banco de dados */

		try {
		PreparedStatement statement = con.prepareStatement(sql);

		/* preenche com os dados */
		statement.setString(1, curso.getNome());
		statement.setInt(2, curso.getNotaMec());
		statement.setFloat(3, curso.getNotaCorte());
		statement.setInt(4, curso.getCod_Universidade());




		/* executa o comando no banco */
		statement.executeUpdate();
		       status = true;

						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricadeConexao.closeConnection(con);		
		}
		
		return status;
	}

	public Curso PesquisaCurso(int Cod){

		Connection con = FabricadeConexao.getConnection(modelo);

		Curso curso = new Curso();

		/* testa se conseguiu conectar */
		if(con == null){
		JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
		return null;
		}	

		/* prepara o sql para inserir no banco */	
		String sql = "SELECT * FROM curso WHERE Cod="+Cod+";";
		

	
	
	/* prepara para inserir no banco de dados */
	
	try {
		PreparedStatement statement = con.prepareStatement(sql);
		
		ResultSet result = statement.executeQuery();
		
		
		
		if(result.next()){
		/* preenche com os dados */
			
		curso.setNome(result.getString("Nome"));
		curso.setNotaMec(result.getInt("Nota MEC"));
		curso.setNotaCorte(result.getFloat("Nota de Corte"));
		curso.setCod_Universidade(result.getInt("Codigo Universidade"));

		
		}else{
			return null;
		}
		/* executa o comando no banco */
		//statement.executeUpdate();		
					
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		
	} finally {
		// Fecha a conexao 
		FabricadeConexao.closeConnection(con);	
	}
	return curso;
}

}
