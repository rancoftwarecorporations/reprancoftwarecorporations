package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import Controller.FabricadeConexao;
import Model.Estudante;
import Model.Interesse;
import Model.ModeloMySql;

public class TabelaInteresseDAO {

	
private ModeloMySql modelo = null;
	
	public TabelaInteresseDAO(){
		
		modelo = new ModeloMySql();
		
		/* o que eu preciso para conectar no banco */
		modelo.setDatabase("rancoftware");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");

	}


	public boolean Inserir(Interesse interesse){
	
		boolean status = true;
		Connection con = FabricadeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
			JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
			return false;
		}		
		
		/* prepara o sql para inserir no banco */
		
		String sql = "INSERT INTO interesse (login_Estudante,cod_Curso) "
				+ "VALUES(?,?);";
		
		
		/* prepara para inserir no banco de dados */
		
		try {
			PreparedStatement statement = con.prepareStatement(sql);
			
			/* preenche com os dados */
			statement.setString(1, interesse.getLogin_estudante());
			statement.setInt(2, interesse.getCod_curso());
			
			
			/* executa o comando no banco */
			statement.executeUpdate();
	        status = true;
			
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricadeConexao.closeConnection(con);		
		}
		
		return status;
	}

public Estudante Pesquisar(String Login){
	
	Connection con = FabricadeConexao.getConnection(modelo);

	Estudante estudante = new Estudante();
	
	/* testa se conseguiu conectar */
	if(con == null){
		JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
		return null;
	}		

	/* prepara o sql para inserir no banco */	
	String sql = "SELECT * FROM estudante WHERE Login="+Login+";";
	
	
	/* prepara para inserir no banco de dados */
	
	try {
		PreparedStatement statement = con.prepareStatement(sql);
		
		ResultSet result = statement.executeQuery();
		
		
		
		if(result.next()){
		/* preenche com os dados */
			
		estudante.setLogin(result.getString("Login"));
		estudante.setSenha(result.getString("Senha"));
		estudante.setNome(result.getString("Nome"));
		estudante.setDataNasc(result.getString("Data de Nascimento"));		
		estudante.setLocal(result.getString("Endereco"));	
		estudante.setNota(result.getFloat("Nota"));		
		estudante.setEmail(result.getString("Email"));	
		}else{
			return null;
		}
		/* executa o comando no banco */
		//statement.executeUpdate();		
					
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		
	} finally {
		// Fecha a conexao 
		FabricadeConexao.closeConnection(con);	
	}
	return estudante;
}
}
