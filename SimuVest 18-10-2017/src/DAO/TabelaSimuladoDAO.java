package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import Controller.FabricadeConexao;
import Model.ModeloMySql;
import Model.Simulado;

public class TabelaSimuladoDAO {
	
private ModeloMySql modelo = null;
	
	public TabelaSimuladoDAO(){
		
		modelo = new ModeloMySql();
		
		/* o que eu preciso para conectar no banco */
		modelo.setDatabase("rancoftware");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");


	}


	public boolean Inserir(Simulado simulado){

		boolean status = true;
		Connection con = FabricadeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
		JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
		return false;
		}	

		/* prepara o sql para inserir no banco */
		
		String sql = "INSERT INTO simulado (area, vestibular, questao, gabarito, nota) "
				+ "VALUES(?,?,?,?,?);";

		
		
		/* prepara para inserir no banco de dados */

		try {
		PreparedStatement statement = con.prepareStatement(sql);

		/* preenche com os dados */
		statement.setString(1, simulado.getArea());
		statement.setString(2, simulado.getVestibular());
		statement.setString(3, simulado.getQuestao());
		statement.setString(4, simulado.getGabarito());
		statement.setFloat(5, simulado.getNota());




		/* executa o comando no banco */
		statement.executeUpdate();
		       status = true;

						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricadeConexao.closeConnection(con);		
		}
		
		return status;
	}

	public Simulado PesquisaSimulado(int Cod){

		Connection con = FabricadeConexao.getConnection(modelo);

		Simulado simulado = new Simulado();

		/* testa se conseguiu conectar */
		if(con == null){
		JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
		return null;
		}	

		/* prepara o sql para inserir no banco */	
		String sql = "SELECT * FROM simulado WHERE Cod="+Cod+";";
		

	
	
	/* prepara para inserir no banco de dados */
	
	try {
		PreparedStatement statement = con.prepareStatement(sql);
		
		ResultSet result = statement.executeQuery();
		
		
		
		if(result.next()){
		/* preenche com os dados */
			
		simulado.setArea(result.getString("Area"));
		simulado.setVestibular(result.getString("Vestibular"));
		simulado.setQuestao(result.getString("Questao"));
		simulado.setGabarito(result.getString("Gabarito"));	
		simulado.setNota(result.getFloat("Nota"));
		
		}else{
			return null;
		}
		/* executa o comando no banco */
		//statement.executeUpdate();		
					
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		
	} finally {
		// Fecha a conexao 
		FabricadeConexao.closeConnection(con);	
	}
	return simulado;
}

}
