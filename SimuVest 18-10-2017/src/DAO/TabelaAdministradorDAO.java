package DAO;

// teste de altera��o do straus

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import Controller.FabricadeConexao;
import Model.Administrador;
import Model.ModeloMySql;


public class TabelaAdministradorDAO {

private ModeloMySql modelo = null;
	
	public TabelaAdministradorDAO(){
		
		modelo = new ModeloMySql();
		
		/* o que eu preciso para conectar no banco */
		modelo.setDatabase("rancoftware");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");

	}


	public boolean Inserir(Administrador administrador){

		boolean status = true;
		Connection con = FabricadeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
		JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
		return false;
		}	

		/* prepara o sql para inserir no banco */
		
		String sql = "INSERT INTO administrador (login, senha, nome, dataNasc, endereco, email) "
				+ "VALUES(?,?,?,?,?,?);";

		
		
		/* prepara para inserir no banco de dados */

		try {
		PreparedStatement statement = con.prepareStatement(sql);

		/* preenche com os dados */
		statement.setString(1, administrador.getLogin());
		statement.setString(2, administrador.getSenha());
		statement.setString(3, administrador.getNome());
		statement.setString(4, administrador.getDataNasc());
		statement.setString(5, administrador.getLocal());
		statement.setString(6, administrador.getEmail());


		/* executa o comando no banco */
		statement.executeUpdate();
		       status = true;

						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricadeConexao.closeConnection(con);		
		}
		
		return status;
	}

	public Administrador PesquisaAdministrador(int Id){

		Connection con = FabricadeConexao.getConnection(modelo);

		Administrador administrador = new Administrador();

		/* testa se conseguiu conectar */
		if(con == null){
		JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
		return null;
		}	

		/* prepara o sql para inserir no banco */	
		String sql = "SELECT * FROM estudante WHERE Id="+Id+";";
		

	
	
	/* prepara para inserir no banco de dados */
	
	try {
		PreparedStatement statement = con.prepareStatement(sql);
		
		ResultSet result = statement.executeQuery();
		
		
		
		if(result.next()){
		/* preenche com os dados */
			
		administrador.setLogin(result.getString("Login"));
		administrador.setSenha(result.getString("Senha"));
		administrador.setNome(result.getString("Nome"));
		administrador.setDataNasc(result.getString("Data de Nascimento"));		
		administrador.setLocal(result.getString("Endereco"));		
		administrador.setEmail(result.getString("Email"));	
		}else{
			return null;
		}
		/* executa o comando no banco */
		//statement.executeUpdate();		
					
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		
	} finally {
		// Fecha a conexao 
		FabricadeConexao.closeConnection(con);	
	}
	return administrador;
}
}
