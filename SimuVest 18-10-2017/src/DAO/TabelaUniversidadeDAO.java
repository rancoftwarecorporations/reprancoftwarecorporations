package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import Controller.FabricadeConexao;
import Model.ModeloMySql;
import Model.Universidade;


public class TabelaUniversidadeDAO {
	
private ModeloMySql modelo = null;
	
	public TabelaUniversidadeDAO(){
		
		modelo = new ModeloMySql();
		
		/* o que eu preciso para conectar no banco */
		modelo.setDatabase("rancoftware");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");

	}


	public boolean Inserir(Universidade universidade){

		boolean status = true;
		Connection con = FabricadeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if(con == null){
		JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
		return false;
		}	

		/* prepara o sql para inserir no banco */
		
		String sql = "INSERT INTO universidade (nome, endereco, cep) "
				+ "VALUES(?,?,?);";

		
		
		/* prepara para inserir no banco de dados */

		try {
		PreparedStatement statement = con.prepareStatement(sql);

		/* preenche com os dados */
		statement.setString(1, universidade.getNome());
		statement.setString(2, universidade.getEndereco());
		statement.setInt(3, universidade.getCEP());

		/* executa o comando no banco */
		statement.executeUpdate();
		       status = true;

						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao 
			FabricadeConexao.closeConnection(con);		
		}
		
		return status;
	}

	public Universidade PesquisaUniversidade(int Cod){

		Connection con = FabricadeConexao.getConnection(modelo);

		Universidade universidade = new Universidade();

		/* testa se conseguiu conectar */
		if(con == null){
		JOptionPane.showMessageDialog(null,"Erro de conex�o ao banco");
		return null;
		}	

		/* prepara o sql para inserir no banco */	
		String sql = "SELECT * FROM universidade WHERE Cod="+Cod+";";
		

	
	
	/* prepara para inserir no banco de dados */
	
	try {
		PreparedStatement statement = con.prepareStatement(sql);
		
		ResultSet result = statement.executeQuery();
		
		
		
		if(result.next()){
		/* preenche com os dados */
			
		universidade.setNome(result.getString("Nome"));
		universidade.setEndereco(result.getString("Endere�o"));
		universidade.setCEP(result.getInt("CEP"));
		
		
		}else{
			return null;
		}
		/* executa o comando no banco */
		//statement.executeUpdate();		
					
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		
	} finally {
		// Fecha a conexao 
		FabricadeConexao.closeConnection(con);	
	}
	return universidade;
}

}
