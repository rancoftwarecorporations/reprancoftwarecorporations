package Model;
public class Interesse {
	private String login_estudante;
	private int cod_curso;
	public Interesse(String login_estudante, int cod_curso) {
		super();
		this.login_estudante = login_estudante;
		this.cod_curso = cod_curso;
	}
	public Interesse() {
		super();
	}
	public String getLogin_estudante() {
		return login_estudante;
	}
	public void setLogin_estudante(String login_estudante) {
		this.login_estudante = login_estudante;
	}
	public int getCod_curso() {
		return cod_curso;
	}
	public void setCod_curso(int cod_curso) {
		this.cod_curso = cod_curso;
	}
	
}
