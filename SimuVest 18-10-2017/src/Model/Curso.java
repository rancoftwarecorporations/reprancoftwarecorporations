package Model;

public class Curso {
	
	private String Nome;
	float notaCorte;
	int notaMec;
	private int cod;
	int cod_Universidade;
	public Curso(String nome, float notaCorte, int notaMec, int cod, int cod_Universidade) {
		super();
		Nome = nome;
		this.notaCorte = notaCorte;
		this.notaMec = notaMec;
		this.cod = cod;
		this.cod_Universidade = cod_Universidade;
	}
	
	public Curso() {
		super();
	}

	public String getNome() {
		return Nome;
	}
	public void setNome(String nome) {
		Nome = nome;
	}
	public float getNotaCorte() {
		return notaCorte;
	}
	public void setNotaCorte(float notaCorte) {
		this.notaCorte = notaCorte;
	}
	public int getNotaMec() {
		return notaMec;
	}
	public void setNotaMec(int notaMec) {
		this.notaMec = notaMec;
	}
	public int getCod() {
		return cod;
	}
	public void setCod(int cod) {
		this.cod = cod;
	}
	public int getCod_Universidade() {
		return cod_Universidade;
	}
	public void setCod_Universidade(int cod_Universidade) {
		this.cod_Universidade = cod_Universidade;
	}
	
	


}