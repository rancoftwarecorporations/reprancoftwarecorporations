package Model;

public class Administrador {
	String login;
	String senha;
	 String nome;
	 String dataNasc;
	 String local;
	 String email;
	 float nota;
	 private int ID;
	public Administrador(String login, String senha, String nome, String dataNasc, String local, String email, float nota,
			int iD) {
		super();
		this.login = login;
		this.senha = senha;
		this.nome = nome;
		this.dataNasc = dataNasc;
		this.local = local;
		this.email = email;
		this.nota = nota;
		ID = iD;
	}
	public Administrador() {
		super();
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDataNasc() {
		return dataNasc;
	}
	public void setDataNasc(String dataNasc) {
		this.dataNasc = dataNasc;
	}
	public String getLocal() {
		return local;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public float getNota() {
		return nota;
	}
	public void setNota(float nota) {
		this.nota = nota;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	
	 
}