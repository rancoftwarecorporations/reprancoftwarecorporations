package Model;

public class Simulado {
	private String area;
	private String vestibular;
	private String questao;
	private String gabarito;
	private float nota;
	private String cod;
	public Simulado(String area, String vestibular, String questao, String gabarito, float nota, String cod) {
		this.area = area;
		this.vestibular = vestibular;
		this.questao = questao;
		this.gabarito = gabarito;
		this.nota = nota;
		this.cod = cod;
	}
	
	public Simulado() {
		super();
	}

	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getVestibular() {
		return vestibular;
	}
	public void setVestibular(String vestibular) {
		this.vestibular = vestibular;
	}
	public String getQuestao() {
		return questao;
	}
	public void setQuestao(String questao) {
		this.questao = questao;
	}
	public String getGabarito() {
		return gabarito;
	}
	public void setGabarito(String gabarito) {
		this.gabarito = gabarito;
	}
	public float getNota() {
		return nota;
	}
	public void setNota(float nota) {
		this.nota = nota;
	}
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	
}