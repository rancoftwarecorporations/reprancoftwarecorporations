package Model;

public class Estudante {

	
	private String Login;
	protected String Senha;
	public String Nome;
	public String DataNasc;
	public String Local;
	public String Email;
	public float nota;
	public Estudante(String login, String senha, String nome, String dataNasc, String local, String email, float nota) {
		super();
		Login = login;
		Senha = senha;
		Nome = nome;
		DataNasc = dataNasc;
		Local = local;
		Email = email;
		this.nota = nota;
	}
	public Estudante() {
		super();
	}
	public String getLogin() {
		return Login;
	}
	public void setLogin(String login) {
		Login = login;
	}
	public String getSenha() {
		return Senha;
	}
	public void setSenha(String senha) {
		Senha = senha;
	}
	public String getNome() {
		return Nome;
	}
	public void setNome(String nome) {
		Nome = nome;
	}
	public String getDataNasc() {
		return DataNasc;
	}
	public void setDataNasc(String dataNasc) {
		DataNasc = dataNasc;
	}
	public String getLocal() {
		return Local;
	}
	public void setLocal(String local) {
		Local = local;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public float getNota() {
		return nota;
	}
	public void setNota(float nota) {
		this.nota = nota;
	}
}