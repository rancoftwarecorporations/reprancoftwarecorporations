package Model;

public class Universidade {
	
	public String Nome;
	public String Endereco;
	public int CEP;
	private int Cod;
	
	
	public Universidade(String nome, String endereco, int cep, int cod) {
		super();
		Nome = nome;
		Endereco = endereco;
		CEP = cep;
		Cod = cod;
	}
	
	
	
	public Universidade() {
		super();
	}



	public String getNome() {
		return Nome;
	}


	public void setNome(String nome) {
		Nome = nome;
	}


	public String getEndereco() {
		return Endereco;
	}


	public void setEndereco(String endereco) {
		Endereco = endereco;
	}


	public int getCEP() {
		return CEP;
	}


	public void setCEP(int cEP) {
		CEP = cEP;
	}


	public int getCod() {
		return Cod;
	}


	public void setCod(int cod) {
		Cod = cod;
	}

}